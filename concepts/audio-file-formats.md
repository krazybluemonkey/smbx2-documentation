# Audio File Formats

While in the past, SMBX only allowed for the playback of MP3 files, the SDL Mixer X plugin has expanded the range of playable audio formats vastly. [See here](https://wohlsoft.ru/pgewiki/SDL_Mixer_X) for a detailed explanation.