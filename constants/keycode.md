# Key Code constants

Constants for player input keys. Only used by onKeyDown/onKeyUp, and thus hardly useful, as player.keys is more convenient to use.

| Constant | Description |
| --- | --- |
| KEY_UP | Up d-pad input. |
| KEY_DOWN | Down d-pad input. |
| KEY_LEFT | Left d-pad input. |
| KEY_RIGHT | Right d-pad input. |
| KEY_JUMP | Regular jump input. |
| KEY_SPINJUMP | Spinjump/alt jump input. |
| KEY_X | Alt run/tanooki input. |
| KEY_RUN | Run input. |
| KEY_SEL | Select/drop item input. |
| KEY_STR | Start/pause input. |